# -*- coding: utf-8 -*-
"""
Flask-JSONConfig
----------------

JSON file as storage of configuration options for flask app.
"""
from setuptools import setup


setup(
    name='Flask-JSONConfig',
    version='1.0.3.2',
    url='',
    license='MIT',
    author='Marcin Wojtysiak',
    author_email='wojtysiak.marcin@gmail.com',
    description='JSON file as storage of configuration options for flask app.',
    long_description=__doc__,
    packages=['flask_JSONConfig'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[        
        'Flask>=0.10'
    ],
    tests_require=[
        'coverage'
    ],
    test_suite='test_JSONConfig',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
