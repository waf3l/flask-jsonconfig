import unittest
import coverage

cov = coverage.coverage()
cov.start()

from flask import Flask
from flask_JSONConfig import JSONConfig
import os
app = Flask(__name__)


class SetupTestFlaskJSONConfig(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        cov.stop()
        cov.report(include='flask_JSONConfig/__init__.py')

    def setUp(self):
        self.app = app.test_client()

    def tearDown(self):
        if os.path.exists('config.json'):
            os.remove('config.json')

    @staticmethod
    def set_config_file():
        with open('test_config.json', 'r') as f:
            content = f.read()

        with open('config.json', 'w') as f:
            f.write(content)


class TestFlaskJSONConfig(SetupTestFlaskJSONConfig):

    def test_create_obj_no_config_file(self):
        with self.assertRaises(OSError):
            config = JSONConfig(app, './', 'config.json')

    def test_create_obj_with_config_file(self):
        self.set_config_file()
        config = JSONConfig(app, './', 'config.json')
        self.assertTrue(app.config['SECRET_KEY'] == 'flask_JSONConfig')

    def test_init_app_obj_no_config_file(self):
        config = JSONConfig('./', 'config.json')
        with self.assertRaises(OSError):
            config.init_app(app)

    def test_init_app_obj_with_config_file(self):
        config = JSONConfig('./', 'config.json')
        self.set_config_file()
        config.init_app(app)
        self.assertTrue(app.config['SECRET_KEY'] == 'flask_JSONConfig')


if __name__ == '__main__':
    unittest.main()
