# -*- coding: utf-8 -*-

import os
import json


class ConfigObj(object):
    """
    Object for config data
    """
    pass


class JSONConfig(object):
    """
    Read json file with configuration options and pass it to flask app
    """

    def __init__(self, app=None):
        """
        Initialize
        """
        self.config_file_name = None

        self.config_location_path = None

        self.config_obj = None

        self.config_data = None

        self.app = app

    def init_app(self, app):
        """
        Init app
        """
        # assign local app object
        self.app = app

    def register_config_file(self, config_file_name, config_location_path):
        """Register config file name and config location path"""
        # config file name
        self.config_file_name = config_file_name

        # config location path
        self.config_location_path = config_location_path

    def load_config(self):
        """Load config data from config file populate data in flask app"""

        if self.config_file_name is None or self.config_location_path is None:
            raise ValueError('You need first to register config file name and config location path')

        # load json data into config_data dict
        self._load_config()

        # create config object
        self.config_obj = self._create_config_obj()

        # load flask config from config_obj
        self.app.config.from_object(self.config_obj)

    def _create_config_obj(self):
        """
        Creates object from config data
        """
        # create empty config object
        conf_obj = ConfigObj()

        config_mode = os.getenv('FLASK_APP_MODE', None)

        if config_mode is None:
            # assign flask app keys and values
            config_mode = self.config_data.get('config', None)

            if config_mode is None:
                raise TypeError('Can not find config key in configuration file')

        env_config = self.config_data.get('env', None)

        if env_config is None:
            raise TypeError('Can not find env key in configuration file')

        config_items = env_config.get(config_mode, None)

        if config_items is None:
            raise TypeError('Can not find items in env key assigned to config mode key in configuration file')

        # flask configuration item make all upper case
        for key in config_items.keys():
            setattr(conf_obj, key.upper(), config_items[key])

        # search config data for additional configuration items
        for key in self.config_data.keys():
            if (key != 'config') and (key != 'env'):
                setattr(conf_obj, key.upper(), self.config_data[key])

        DB_URI = os.getenv('DB_URI', None)

        if DB_URI is not None:
            setattr(conf_obj, 'SQLALCHEMY_DATABASE_URI', DB_URI)

        SECRET_KEY = os.getenv('SECRET_KEY', None)

        if SECRET_KEY is not None:
            setattr(conf_obj, 'SECRET_KEY', SECRET_KEY)

        return conf_obj

    def _load_config(self):
        """
        Get the path of config file and loads the config file

        path: the path for search config file
        """
        if not os.path.exists(os.path.join(self.config_location_path, self.config_file_name)):
            raise OSError('Config file not found')

        with open(os.path.join(self.config_location_path, self.config_file_name)) as f:
            self.config_data = json.loads(f.read())
